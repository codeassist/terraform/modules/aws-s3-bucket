# See:
#   * https://www.terraform.io/docs/providers/aws/r/s3_bucket.html

locals {
  enabled         = var.s3_bucket_module_enabled ? true : false
  is_named_bucket = length(var.s3_bucket_name) > 0 ? true : false

  tags = merge(
    var.s3_bucket_tags,
    local.is_named_bucket ? { Name = var.s3_bucket_name } : { Prefix = var.s3_bucket_prefix },
    {
      terraform = "true"
    },
  )

  acceleration_status = var.s3_acceleration_enabled ? "Enabled" : "Suspended"
}


# -----------------------------------------------------------------
# Manage S3 bucket resource (either with specified name or prefix)
# -----------------------------------------------------------------
resource "aws_s3_bucket" "named_bucket" {
  count = (local.enabled && local.is_named_bucket) ? 1 : 0

  # (Optional, Forces new resource) The name of the bucket. If omitted, Terraform will assign a random, unique name.
  bucket = var.s3_bucket_name
  # (Optional) The canned ACL to apply.
  acl = var.s3_bucket_acl
  # (Optional) A valid bucket policy JSON document. Note that if the policy document is not specific enough (but
  # still valid), Terraform may view the policy as constantly changing in a terraform plan. In this case, please make
  # sure you use the verbose/specific version of the policy.
  policy = var.s3_bucket_policy
  # (Optional) A mapping of tags to assign to the bucket.
  tags = local.tags
  # (Optional) A boolean that indicates all objects should be deleted from the bucket so that the bucket can be
  # destroyed without error. These objects are not recoverable.
  force_destroy = var.s3_bucket_force_destroy

  # A state of versioning object:
  versioning {
    # (Optional) Enable versioning. Once you version-enable a bucket, it can never return to an unversioned state.
    # You can, however, suspend versioning on that bucket.
    enabled = var.s3_bucket_versioning_enabled
  }

  # (Optional) Sets the accelerate configuration of an existing bucket. Can be:
  #   * Enabled
  #   * Suspended
  acceleration_status = local.acceleration_status

  # (Optional) A configuration of server-side encryption, see more at:
  #   * https://docs.aws.amazon.com/AmazonS3/latest/dev/bucket-encryption.html
  server_side_encryption_configuration {
    # (required) A single object for server-side encryption by default configuration.
    rule {
      # (required) A single object for setting server-side encryption by default.
      apply_server_side_encryption_by_default {
        # (required) The server-side encryption algorithm to use. Valid values are "AES256" and "aws:kms"
        sse_algorithm = var.s3_bucket_encryption_algorithm

        # (optional) The AWS KMS master key ID used for the SSE-KMS encryption. This can only be used when you set
        # the value of `sse_algorithm` as "aws:kms". The default "aws/s3" AWS KMS master key is used if this element
        # is absent while the `sse_algorithm` is "aws:kms".
        kms_master_key_id = var.s3_bucket_kms_master_key_id
      }
    }
  }
}

resource "aws_s3_bucket" "prefixed_bucket" {
  count = (local.enabled && ! local.is_named_bucket) ? 1 : 0

  # (Optional, Forces new resource) Creates a unique bucket name beginning with the specified prefix. Conflicts
  # with `bucket`.
  bucket_prefix = var.s3_bucket_prefix
  # (Optional) The canned ACL to apply.
  acl = var.s3_bucket_acl
  # (Optional) A valid bucket policy JSON document. Note that if the policy document is not specific enough (but
  # still valid), Terraform may view the policy as constantly changing in a terraform plan. In this case, please make
  # sure you use the verbose/specific version of the policy.
  policy = var.s3_bucket_policy
  # (Optional) A mapping of tags to assign to the bucket.
  tags = local.tags
  # (Optional) A boolean that indicates all objects should be deleted from the bucket so that the bucket can be
  # destroyed without error. These objects are not recoverable.
  force_destroy = var.s3_bucket_force_destroy

  # A state of versioning object:
  versioning {
    # (Optional) Enable versioning. Once you version-enable a bucket, it can never return to an unversioned state.
    # You can, however, suspend versioning on that bucket.
    enabled = var.s3_bucket_versioning_enabled
  }

  # (Optional) Sets the accelerate configuration of an existing bucket. Can be:
  #   * Enabled
  #   * Suspended
  acceleration_status = local.acceleration_status

  # (Optional) A configuration of server-side encryption, see more at:
  #   * https://docs.aws.amazon.com/AmazonS3/latest/dev/bucket-encryption.html
  server_side_encryption_configuration {
    # (required) A single object for server-side encryption by default configuration.
    rule {
      # (required) A single object for setting server-side encryption by default.
      apply_server_side_encryption_by_default {
        # (required) The server-side encryption algorithm to use. Valid values are "AES256" and "aws:kms".
        sse_algorithm = var.s3_bucket_encryption_algorithm

        # (optional) The AWS KMS master key ID used for the SSE-KMS encryption. This can only be used when you set
        # the value of `sse_algorithm` as "aws:kms". The default "aws/s3" AWS KMS master key is used if this element
        # is absent while the `sse_algorithm` is "aws:kms".
        kms_master_key_id = var.s3_bucket_kms_master_key_id
      }
    }
  }
}


# ----------------------------------------------------------------------
# Add strict bucket policy if it's required to encrypt uploaded objects
# ----------------------------------------------------------------------
data "aws_iam_policy_document" "force_encryption" {
  count = (local.enabled && var.s3_bucket_allow_encrypted_uploads_only) ? 1 : 0

  statement {
    sid    = "DenyIncorrectEncryptionHeader"
    effect = "Deny"
    actions = [
      "s3:PutObject"
    ]
    resources = [
      "${local.is_named_bucket ? join("", aws_s3_bucket.named_bucket.*.arn) : join("", aws_s3_bucket.prefixed_bucket.*.arn)}/*"
    ]
    principals {
      identifiers = ["*"]
      type        = "*"
    }
    condition {
      test     = "StringNotEquals"
      values   = [var.s3_bucket_encryption_algorithm]
      variable = "s3:x-amz-server-side-encryption"
    }
  }

  statement {
    sid    = "DenyUnEncryptedObjectUploads"
    effect = "Deny"
    actions = [
      "s3:PutObject"
    ]
    resources = [
      "${local.is_named_bucket ? join("", aws_s3_bucket.named_bucket.*.arn) : join("", aws_s3_bucket.prefixed_bucket.*.arn)}/*"
    ]
    principals {
      identifiers = ["*"]
      type        = "*"
    }
    condition {
      test     = "Null"
      values   = ["true"]
      variable = "s3:x-amz-server-side-encryption"
    }
  }
}

resource "aws_s3_bucket_policy" "force_encryption" {
  count = (local.enabled && var.s3_bucket_allow_encrypted_uploads_only) ? 1 : 0

  # (Required) The name of the bucket to which to apply the policy.
  bucket = local.is_named_bucket ? join("", aws_s3_bucket.named_bucket.*.id) : join("", aws_s3_bucket.prefixed_bucket.*.id)
  # (Required) The text of the policy.
  policy = join("", data.aws_iam_policy_document.force_encryption.*.json)
}
