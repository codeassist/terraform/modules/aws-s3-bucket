aws-s3-bucket
=============

Creates a simple S3 bucket (if its name is specified), optionally with versioning and encryption.

## Usage

```hcl-terraform
module "aws-s3-bucket" {
  source        = "git::https://gitlab.com/codeassist/terraform/modules/aws-s3-bucket.git?ref=<tag>"
  bucket        = "my-bucket-name"
  tags {
    Environment = "Dev"
  }
}
```

Example policy for Terraform remote S3 backend bucket:
```hcl
...
data "aws_iam_policy_document" "bucket_policy_definition" {
  statement {
    sid = "RequireEncryptedTransport"
    effect = "Deny"
    actions = [
      "s3:*",
    ]
    resources = [
      "${aws_s3_bucket.tfstate_backend_bucket.arn}/*"
    ]
    condition {
      test = "Bool"
      variable = "aws:SecureTransport"
      values = [
        false,
      ]
    }
    principals {
      type = "*"
      identifiers = ["*"]
    }
  }
  statement {
    sid = "DenyIncorrectEncryptionHeader"
    effect = "Deny"
    actions = [
      "s3:PutObject",
    ]
    resources = [
      "${aws_s3_bucket.tfstate_backend_bucket.arn}/*"
    ]
    condition {
      test = "StringNotEquals"
      variable = "s3:x-amz-server-side-encryption"
      values = [
        "AES256"
      ]
    }
    principals {
      type = "*"
      identifiers = ["*"]
    }
  }
  statement {
    sid = "DenyUnEncryptedObjectUploads"
    effect = "Deny"
    actions = [
      "s3:PutObject",
    ]
    resources = [
      "${aws_s3_bucket.tfstate_backend_bucket.arn}/*"
    ]
    condition {
      test = "Null"
      variable = "s3:x-amz-server-side-encryption"
      values = [
        "true"
      ]
    }
    principals {
      type = "*"
      identifiers = ["*"]
    }
  }
}
```


## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-------:|:--------:|
| s3_bucket_name | The name of the bucket. | string | `""` | no |
| s3_bucket_tags | A mapping of tags to assign to the bucket. | map | `{ Name: "${var.bucket}", terraform: "true" }` | no |
```See variables' descriptions inside variables.tf```


## Outputs

| Name | Description |
|------|-------------|
| s3_bucket_arn | The ARN of the bucket. Will be of format arn:aws:s3:::bucketname. |
```See outputs' descriptions inside outputs.tf```
