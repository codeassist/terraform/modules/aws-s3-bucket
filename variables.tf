variable "s3_bucket_module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # "false" prevents the module from creating any resources
  default = false
}

variable "s3_bucket_name" {
  description = "The name of the bucket."
  # If omitted, Terraform will assign a random, unique name.
  type    = string
  default = ""
}

variable "s3_bucket_prefix" {
  description = "Creates a unique bucket name beginning with the specified prefix. Conflicts with `bucket`."
  type        = string
  default     = ""
}

variable "s3_bucket_acl" {
  description = "The canned ACL to apply."
  type        = string
  default     = null
}

variable "s3_bucket_policy" {
  description = "A valid bucket policy JSON document."
  # NOTE(!): that if the policy document is not specific enough (but still valid), Terraform may view the policy as
  # constantly changing in a terraform plan. In this case, please make sure you use the verbose/specific version of
  # the policy.
  type    = string
  default = null
}

variable "s3_bucket_tags" {
  description = "A mapping of tags to assign to the bucket."
  type        = map(string)
  default     = {}
}

variable "s3_bucket_force_destroy" {
  description = "A boolean string that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error."
  # Objects deleted from the S3 bucket are not recoverable.
  type    = bool
  default = false
}

variable "s3_bucket_versioning_enabled" {
  description = "A state of versioning."
  # Versioning is a means of keeping multiple variants of an object in the same bucket.
  type    = bool
  default = false
}

variable "s3_acceleration_enabled" {
  description = "Whether to enabled the accelerate configuration on an existing bucket."
  type        = bool
  default     = false
}

variable "s3_bucket_encryption_algorithm" {
  description = "The server-side encryption algorithm to use. Valid values are `AES256` and `aws:kms`"
  type        = string
  default     = "AES256"
}

variable "s3_bucket_kms_master_key_id" {
  description = "The AWS KMS master key ID used for the `SSE-KMS` encryption."
  # This can only be used when you set the value of `sse_algorithm` as "aws:kms". The default "aws/s3" AWS KMS
  # master key is used if this element is absent while the `sse_algorithm` is "aws:kms".
  type    = string
  default = null
}

variable "s3_bucket_allow_encrypted_uploads_only" {
  description = "Set to `true` to prevent uploads of unencrypted objects to S3 bucket"
  type        = bool
  default     = false
}
