# Whether to create the resources ("false" prevents the module from creating any resources).
s3_bucket_module_enabled = true

# The legacy rules for bucket names in the US East (N. Virginia) Region allowed bucket names to be as long as
# 255 characters, and bucket names could contain any combination of uppercase letters, lowercase letters, numbers,
# periods (.), hyphens (-), and underscores (_).
#
# The name of the bucket used for Amazon S3 Transfer Acceleration must be DNS-compliant and must not contain periods (".").

# The name of the bucket (If omitted, Terraform will assign a random, unique name).
s3_bucket_name = "test2"
# Creates a unique bucket name beginning with the specified prefix. Conflicts with `bucket`.
s3_bucket_prefix = ""

# The canned ACL to apply.
s3_bucket_acl = null
# A valid bucket policy JSON document.
# NOTE(!): that if the policy document is not specific enough (but still valid), Terraform may view the policy as
# constantly changing in a terraform plan. In this case, please make sure you use the verbose/specific version of
# the policy.
s3_bucket_policy = null
# A mapping of tags to assign to the bucket.
s3_bucket_tags = {}
# A boolean string that indicates all objects should be deleted from the bucket so that the bucket can be
# destroyed without error. Objects deleted from the S3 bucket are not recoverable.
s3_bucket_force_destroy = true
# A state of versioning. Versioning is a means of keeping multiple variants of an object in the same bucket.
s3_bucket_versioning_enabled = true
# Whether to enabled the accelerate configuration on an existing bucket.
s3_acceleration_enabled = false
# The server-side encryption algorithm to use. Valid values are "AES256" and "aws:kms".
s3_bucket_encryption_algorithm = "AES256"
# The AWS KMS master key ID used for the `SSE-KMS` encryption. This can only be used when you set the value
# of `sse_algorithm` as "aws:kms". The default "aws/s3" AWS KMS master key is used if this element is absent
# while the `sse_algorithm` is "aws:kms".
s3_bucket_kms_master_key_id = null
# Set to "true" to prevent uploads of unencrypted objects to S3 bucket
s3_bucket_allow_encrypted_uploads_only = false
